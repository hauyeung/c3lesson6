﻿namespace csharp3_lesson6
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fahrenheittextbox = new System.Windows.Forms.TextBox();
            this.celsiustextbox = new System.Windows.Forms.TextBox();
            this.kelvintextbox = new System.Windows.Forms.TextBox();
            this.closeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fahrenheit";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Celsius";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Kelvin";
            // 
            // fahrenheittextbox
            // 
            this.fahrenheittextbox.Location = new System.Drawing.Point(107, 39);
            this.fahrenheittextbox.Name = "fahrenheittextbox";
            this.fahrenheittextbox.Size = new System.Drawing.Size(100, 20);
            this.fahrenheittextbox.TabIndex = 3;
            this.fahrenheittextbox.Leave += new System.EventHandler(this.fahrenheittextbox_Leave);
            // 
            // celsiustextbox
            // 
            this.celsiustextbox.Location = new System.Drawing.Point(107, 80);
            this.celsiustextbox.Name = "celsiustextbox";
            this.celsiustextbox.Size = new System.Drawing.Size(100, 20);
            this.celsiustextbox.TabIndex = 4;
            this.celsiustextbox.Leave += new System.EventHandler(this.celsiustextbox_Leave);
            // 
            // kelvintextbox
            // 
            this.kelvintextbox.Location = new System.Drawing.Point(107, 117);
            this.kelvintextbox.Name = "kelvintextbox";
            this.kelvintextbox.Size = new System.Drawing.Size(100, 20);
            this.kelvintextbox.TabIndex = 5;
            this.kelvintextbox.Leave += new System.EventHandler(this.kelvintextbox_Leave);
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(107, 156);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 6;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 297);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.kelvintextbox);
            this.Controls.Add(this.celsiustextbox);
            this.Controls.Add(this.fahrenheittextbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "Temperature Conversion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox fahrenheittextbox;
        private System.Windows.Forms.TextBox celsiustextbox;
        private System.Windows.Forms.TextBox kelvintextbox;
        private System.Windows.Forms.Button closeButton;
    }
}

