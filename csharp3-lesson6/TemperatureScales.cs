﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace csharp3_lesson6
{
    
       public struct Fahrenheit
        {
            // Public field
            public float Temperature;
            // Constructor
            public Fahrenheit(float temperature)
            {
                Temperature = temperature;
            }
            // Implicit cast from float to Fahrenheit
            public static implicit operator Fahrenheit(float temperature)
            {
                return new Fahrenheit(temperature);
            }
            // Implicit cast from Celsius to Fahrenheit
            public static implicit operator Fahrenheit(Celsius celsius)
            {
                return new Fahrenheit((celsius.Temperature * (5.0F / 9.0F)) + 32.0F);
            }
            // Explicit cast from Fahrenheit to long
            public static explicit operator long(Fahrenheit fahrenheit)
            {
                return (long)Math.Round(fahrenheit.Temperature, 0);
            }
            // Method to return Fahrenheit to Celsius conversion
            public float Celsius()
            {
                return ((Temperature - 32.0F) * (5.0F / 9.0F));
            }

            // Method to return Fahrenheit to Celsius conversion
            public float Kelvin()
            {
                return ((Temperature - 32.0F) * (5.0F / 9.0F)+273.15F);
            }
            // Display Fahrenheit value as a string
            public override string ToString()
            {
                return Temperature.ToString();
            }
       }

        public struct Celsius
        {
            // Public field
            public float Temperature;
            // Constructor
            public Celsius(float temperature)
            {
                Temperature = temperature;
            }
            // Implicit cast from float to Celsius
            public static implicit operator Celsius(float temperature)
            {
                return new Celsius(temperature);
            }
            // Implicit cast from Fahrenheit to Celsius
            public static implicit operator Celsius(Fahrenheit fahrenheit)
            {
                return new Celsius((fahrenheit.Temperature - 32.0F) * (5.0F / 9.0F));
            }
            // Explicit cast from Celsius to long
            public static explicit operator long(Celsius celsius)
            {
                    return (long)Math.Round(celsius.Temperature, 0);
            }
            // Method to return Celsius to Fahrenheit conversion
            public float Fahrenheit()
            {
                return ((Temperature * (9.0F / 5.0F)) + 32);
            }
            // Method to return Celsius to Kelvin conversion
            public float Kelvin()
            {
                return (Temperature +273.15F);
            }
         

            // Display Celsius value as a string
            public override string ToString()
            {
                return Temperature.ToString();
            }
        }

        public struct Kelvin
        {
            // Public field
            public float Temperature;
            // Constructor
            public Kelvin(float temperature)
            {
                Temperature = temperature;
            }
            // Implicit cast from float to Celsius
            public static implicit operator Kelvin(float temperature)
            {
                return new Kelvin(temperature);
            }
            // Implicit cast from Fahrenheit to Kelvin
            public static implicit operator Kelvin(Fahrenheit fahrenheit)
            {
                return new Kelvin(fahrenheit.Temperature * (5.0F / 9.0F)+273.15F);
            }
            // Explicit cast from Kelvin to long
            public static explicit operator long(Kelvin kelvin)
            {
                return (long)Math.Round(kelvin.Temperature, 0);
            }
            // Method to return Kelvin to Fahrenheit conversion
            public float Fahrenheit()
            {
                return ((Temperature - 273.15F) *(9.0F/5.0F)+32F);
            }

            // Method to return Kelvin to Celsius conversion
            public float Celsius()
            {
                return (Temperature - 273.15F);
            }
            // Display Celsius value as a string
            public override string ToString()
            {
                return Temperature.ToString();
            }
        }


}
