﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp3_lesson6
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void fahrenheittextbox_Leave(object sender, EventArgs e)
        {
            float f = 0.0f;
            if (float.TryParse(fahrenheittextbox.Text, out f))
            {
                Fahrenheit fa = new Fahrenheit(f);
                celsiustextbox.Text = fa.Celsius().ToString();
                kelvintextbox.Text = fa.Kelvin().ToString();
            }
            else
            {
                MessageBox.Show("Invalid number");
            }

        }

        private void celsiustextbox_Leave(object sender, EventArgs e)
        {

            float f = 0.0f;
            if (float.TryParse(celsiustextbox.Text, out f))
            {
                Celsius c = new Celsius(f);
                fahrenheittextbox.Text = c.Fahrenheit().ToString();
                kelvintextbox.Text = c.Kelvin().ToString();
            }
            else
            {
                MessageBox.Show("Invalid number");
            }
        }

        private void kelvintextbox_Leave(object sender, EventArgs e)
        {
            float f = 0.0f;
            if (float.TryParse(kelvintextbox.Text, out f))
            {

                Kelvin k = new Kelvin(f);
                celsiustextbox.Text = k.Celsius().ToString();
                fahrenheittextbox.Text = k.Fahrenheit().ToString();
            }
            else
            {
                MessageBox.Show("Invalid number");
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
